import React from 'react';
import {addEntry} from "../client/namesClient";

const AddNameBar = ({name,setName,namesList,setNamesList}) => {

  function handleSubmit(event) {
    event.preventDefault();
     addEntry(name,namesList).then(setNamesList);
     setName("");
  }

  return (
    <div>
      <form onSubmit={handleSubmit} className="row g-3 mt-2">
        <div className="col-auto">
          <input type="text" className="form-control" id="inputName" placeholder="Enter name..."
                 name="newName"
                 value={name}
                 onChange={(e) => setName(e.target.value)}/>
        </div>
        <div className="col-auto">
          <button type="submit" className="btn btn-primary mb-3">Add</button>
        </div>
      </form>
    </div>
  );
}

export default AddNameBar;
