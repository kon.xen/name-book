import React, {useState} from 'react';
import {editEntry, deleteEntry} from "../client/namesClient";
import 'bootstrap/dist/css/bootstrap.min.css'
import DeleteModal from "./deleteModal";

const NamesTableRow = ({id, name, setResults,setName, setNamesList, nameList}) => {

  const [edit, setEdit] = useState(true);
  const [visibility, setVisibility] = useState("invisible");
  const [editName, setEditName] = useState(name);
  const [show, setShow] = useState(false);

  function toggleEdit() {
    !edit ? setEdit(true) : setEdit(false);
    !edit ? setVisibility("invisible") : setVisibility("visible");
  }

  function handleSubmit(event) {
    event.preventDefault();
    editEntry(id, editName, nameList,).then(setNamesList);
    setEdit(false);
    setResults=[];
    window.location.reload();
  }

  function toggleModal() {
    show ? setShow(false) : setShow(true);
  }

  function handleDelete(id) {
    deleteEntry(id, nameList).then(setNamesList);
    toggleModal();
    window.location.reload();
  }

  return (
    <>
      <tr id={`row${id}`}>
        <td>{id}</td>
        <td>
          <form onSubmit={handleSubmit}>
            <div className="input-group">
              <input
                className="form-control"
                type="text"
                disabled={edit}
                value={editName}
                onChange={(e) => setEditName(e.target.value)}/>
              <button type="submit" disabled={edit} className={`btn btn-sm btn-outline-secondary ${visibility}`}>OK
              </button>
            </div>
          </form>
        </td>
        <td>
          <button onClick={toggleEdit} type="button" className="btn btn-sm btn-outline-secondary me-md-2">Edit</button>
          <button onClick={toggleModal} type="button" className="btn btn-sm btn-danger">delete</button>
        </td>
        <DeleteModal show={show} id={id} name={name} toggleModal={toggleModal} handleDelete={handleDelete}/>
      </tr>
    </>
  );
}
export default NamesTableRow;
