import React from 'react';
import NamesTableRow from "./namesTableRow";

const NamesTable = ({namesList, toRenderList, setName, setResults, deleteEntry, putInOrder}) => {
  return (
    <div>
      <table className="table">
        <thead>
        <tr>
          <th scope="col">
            <button onClick={putInOrder} className="button round"></button>
          </th>
          <th scope="col">
            name
          </th>
          <th scope="col">edit / delete</th>
        </tr>
        </thead>
        <tbody>
        {
          toRenderList.map((entry, id) => {
            return (
              <NamesTableRow key={id}
                             id={id}
                             name={entry}
                             nameList={namesList}
                             setName={setName}
                             deleteEntry={deleteEntry}
                             setResults={setResults}
              />
            );
          })
        }
        </tbody>
      </table>
    </div>
  );
}


export default NamesTable;
