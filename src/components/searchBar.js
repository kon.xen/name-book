import React from 'react';

const SearchBar = ({setSearchTerm, searchTerm, setResults}) => {

  const clearSearch = () => {
    setSearchTerm('');
    setResults([]);
  }

  return (
    <>
      <form className="row g-3 ">
        <div className="col-auto">
          <input type="text" value={searchTerm} className="form-control" id="inputName" placeholder="Search for ...."
                 onChange={(e) => {
                   setSearchTerm(e.target.value);
                 }}/>
        </div>
        <div className="col-auto">
          <button onClick={clearSearch}
                  type="button"
                  className={`form-control btn btn-outline-secondary ${searchTerm ? "visible" : "invisible"}`}>clear
          </button>
        </div>
      </form>
    </>
  );
}

export default SearchBar;
