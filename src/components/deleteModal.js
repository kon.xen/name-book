import React from "react";
import {Modal, Button} from "react-bootstrap";

const DeleteModal = ({id, name, show, toggleModal, handleDelete}) => {
  const confirmation = () => {
    handleDelete(id);
    toggleModal();
  }

  return (
    <>
      <Modal show={show}>
        <Modal.Header closeButton>Delete</Modal.Header>
        <Modal.Body>Delete name: {name}</Modal.Body>
        <Modal.Footer>
          <Button onClick={toggleModal} variant="outline-secondary">Cancel</Button>
          <Button onClick={confirmation} variant="outline-danger">Delete</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default DeleteModal;