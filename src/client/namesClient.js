export async function addEntry(name, list) {
  let tempList = list;
  tempList.push(name);
  localStorage.removeItem('namesList');
  localStorage.setItem('namesList', JSON.stringify(tempList));
  return tempList;
}

export async function editEntry(nameId, newName, namesList) {
  console.log(namesList);
  let tempList = namesList;
  tempList[nameId] = newName;
  localStorage.setItem('namesList', JSON.stringify(tempList));
  return tempList;
}

export async function deleteEntry(nameId, namesList) {
  let tempList = namesList;
  tempList.splice(nameId, 1);
  localStorage.removeItem('namesList');
  localStorage.setItem('namesList', JSON.stringify(tempList));
  return tempList;
}

