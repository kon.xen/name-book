import React, {useEffect, useState} from 'react';
import SearchBar from "./components/searchBar";
import AddNameBar from "./components/addNameBar";
import NamesTable from "./components/namesTable";

const App = () => {

  const [namesList, setNamesList] = useState([]);
  const [nameVal, setNameVal] = useState("");
  const [searchTerm, setSearchTerm] = useState('');
  const [results, setResults] = useState([]);

  function reset() {
    localStorage.removeItem('namesList');
    setNameVal("");
    window.location.reload();
    console.log("reset done");
  }

  function findName() {
    let temp = [];
    console.log("looking for " + searchTerm);
    if (namesList.includes(searchTerm, 0)) {
      console.log("found name: " + searchTerm);
      temp[namesList.indexOf(searchTerm)] = searchTerm;
      setResults(temp);
    }
  }

  function putInOrder() {
    console.log("clicked");
    let temp = namesList;
    let order = temp.sort();
    localStorage.removeItem('namesList');
    localStorage.setItem('namesList', JSON.stringify(order));
    window.location.reload();
  }

  useEffect(() => {
    if (!localStorage.getItem('namesList')) {
      localStorage.setItem('namesList', JSON.stringify(namesList));
    } else {
      setNamesList(JSON.parse(localStorage.getItem('namesList')));
    }
  },[]  );

  useEffect(() => {
    findName();
  }, [searchTerm]);


  return (
    <>
      <main className="container-fluid">

        <div className="row sm mt-4">
          <div className="col"></div>
          <div className="col-auto">
            <h1 className="header">Name Book</h1>
            <SearchBar namesList={namesList}
                       searchTerm={searchTerm}
                       setSearchTerm={setSearchTerm}
                       setResults={setResults}
            />
            <AddNameBar namesList={namesList}
                        setNamesList={setNamesList}
                        name={nameVal}
                        setName={setNameVal}
            />
            <NamesTable namesList={namesList}
                        setNamesList={setNamesList}
                        setResults={setResults}
                        toRenderList={(results.length > 0) ? results : namesList}
                        name={nameVal} setName={setNameVal}
                        putInOrder={putInOrder}
            />

            <button className={`btn btn-sm btn-outline-danger ${namesList.length > 0 ? "visible" : "invisible"}`}
                    type="button" onClick={reset}>
              Reset
            </button>

          </div>
          <div className="col"></div>
        </div>

      </main>
    </>
  );
}

export default App;